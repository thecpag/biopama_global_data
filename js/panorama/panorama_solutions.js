jQuery(document).ready(function($) {
	var solutionsLoaded = 0;
	var currentCode;
	var checkCode;
	var thisScope;

	$('#collapsePanorama').on('shown.bs.collapse', function () {
		if (thisScope == "country") {
			checkCode = selSettings.ISO2
		} else if (thisScope == "pa"){
			checkCode = selSettings.WDPAID
		}
		if ((solutionsLoaded == 0) || (currentCode !== checkCode)){
			solutionsLoaded = 1;
			$( ".panorama-solutions-results" ).empty();
			var panoramaAreaCode = getAreaCode();
			
			var panoramaPoints = [];
			var jqxhr = $.get( "https://panorama.solutions/en/api/v1/solutions?api_key=b15ad7e5b77bc99b0f26a691&"+panoramaAreaCode, function(data) {
				var results = '<div class="container solution-container"><div class="row">';
				//console.log(data.solutions.length)
				if (data.solutions.length){
					$.each(data.solutions, function( index, value ) {
						//we wrap the result in the col class seperately to allow the map popup to appear clean
						results += '<div class="solution-wrapper col-sm-4 col-m-2 col-md-3 col-xs-10 p-2 text-white">';
						var thisResult = '<a class="solution-link" target="_blank" href='+value.solution.url+'>'+
						'<div class="solution-inner p-3">'+
							'<div class="solution-header-picture">'+
								'<img typeof="foaf:Image" src='+value.solution.preview_image.src+' width="380" height="220" title='+value.solution.title+'>'+
							'</div>'+
							'<div class="solution-content">'+
								'<div class="solution-title">'+value.solution.title+'</div>';
								if (value.solution.contributors.length > 0){
									thisResult += '<div class="solution-submit">'+
										'<div class="solution-provider">by <span class="solution-provider-name">'+value.solution.contributors[0].contributor.name+'</span></div>'+
										'<div class="solution-organisation">'+value.solution.contributors[0].contributor.organisation+'</div>'+
									'</div>';
								}
								thisResult += '<div class="solution_location">'+
									'<strong><div class="solution-location-label"><i class="fas fa-map-marker-alt"></i> Location</div></strong>'+
									'<span>'+value.solution.field_location+'</span>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'</a>';
						results += thisResult
						results += '</div>';
						if (value.solution.field_location_geofield !== null){
							var regex = /\[.*?\]/g;
							var solutionPoint = value.solution.field_location_geofield.match(regex) + '';
							solutionPoint = solutionPoint.slice(1, -1);
							var solutionLoc = solutionPoint.split(",");
							var solutioXcoord = parseInt(solutionLoc[0]);
							var solutioYcoord = parseInt(solutionLoc[1]);
							var solutioCoords = [solutioXcoord, solutioYcoord, 0];
							var panoramaSolution = { "type": "Feature", "properties": { "id": index, "contents": '<div class="panorama-marker-popup">'+thisResult+'</div>'}, "geometry": { "type": "Point", "coordinates": solutioCoords } }
							panoramaPoints.push(panoramaSolution)
						}
						
					});
				} else {
					results += '<div class="alert alert-warning" role="alert">' +
						'At present, no solution case studies related to ' + selSettings.paName + ' available on the PANORAMA web platform. Would you like to submit one? Go here: <a href="https://panorama.solutions/en/solution/add">https://panorama.solutions/en/solution/add</a>' +
					'</div>';
				}
				results += '</div></div>'+
				$( ".panorama-solutions-results" ).html( results );
			})
			.done(function() {
				var panoramaGeoJson = {
					"type": "FeatureCollection",
					"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
				};
				panoramaGeoJson.features = panoramaPoints;

				mymap.getSource('panorama').setData(panoramaGeoJson);

				var currentLayers = mymap.style._layers;
				for (var key in currentLayers) { 
					if (currentLayers[key].id.indexOf('panorama-point') != -1) {
						mymap.removeLayer('panorama-point');
					} 
				}	
				mymap.addLayer({
					id: "panorama-point",
					type: "circle",
					source: "panorama",
					layout: {
						"visibility": "none"
					},
					paint: {
						"circle-color": "#11b4da",
						"circle-radius": 6,
						"circle-stroke-width": 1,
						"circle-stroke-color": "#fff"
					}
				});
				mymap.on('click', 'panorama-point', function (e) {
					var coordinates = e.features[0].geometry.coordinates.slice();
					var popText = e.features[0].properties.contents;
					new mapboxgl.Popup({className: 'panorama-popup'})
						.setLngLat(coordinates)
						.setHTML(popText)
						.addTo(mymap);
				});
				//console.log(panoramaPoints)
				mymap.setLayoutProperty("panorama-point", 'visibility', 'visible');
			})
			.fail(function() {
				$( ".panorama-solutions-results" ).html( '<div class="alert alert-warning"><strong>Error!</strong> Panorama Solutions are not working correctly</div>' );
			})
		} else {
			mymap.setLayoutProperty("panorama-point", 'visibility', 'visible');
		}
	});

	$('#collapsePanorama').on('hide.bs.collapse', function () {
		mymap.setLayoutProperty("panorama-point", 'visibility', 'none');	
	});
	
	function getAreaCode(){
		var panoramaCode;
		if (window.location.href.indexOf('ct/pa') > -1){
			panoramaCode = 'wdpa='+selSettings.WDPAID;
			currentCode = selSettings.WDPAID;
			thisScope = 'pa';
		} else if (window.location.href.indexOf('ct/country') > -1){
			panoramaCode = 'country_iso_2='+selSettings.ISO2;
			currentCode = selSettings.ISO2;
			thisScope = 'country';
		} else {
			panoramaCode = "";
		}
		return panoramaCode;
	}
});